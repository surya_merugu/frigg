/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_timer_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_timer_init.h"

TIM_HandleTypeDef htim3_t, htim2_t;

static RE_StatusTypeDef HAL_TIM_Clk_Init (TIM_HandleTypeDef* htim_base);

/**
  * @Brief RE_Timer_ClkInit
  * This function configures clock and set interrupt priority for the timer
  * @Param htim_base: TIM_HandleTypeDef
  * @Retval Exit status
  */
static RE_StatusTypeDef HAL_TIM_Clk_Init (TIM_HandleTypeDef* htim_base)
{   
    if(htim_base->Instance == TIM3)
    {
        __HAL_RCC_TIM3_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(TIM3_IRQn);
    }
    else if(htim_base->Instance == TIM2)
    {
        __HAL_RCC_TIM2_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(TIM2_IRQn);        
    }
    return RE_OK;
}

/**
  * @Brief RE_TIMER3_Init
  * This function configures Timer3 peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_TIMER3_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim3_t.Instance                          = TIM3;
    if(HAL_TIM_Clk_Init(&htim3_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    htim3_t.Init.Prescaler                    = 49999;
    htim3_t.Init.CounterMode                  = TIM_COUNTERMODE_UP;
    htim3_t.Init.Period                       = 899;
    htim3_t.Init.ClockDivision                = TIM_CLOCKDIVISION_DIV1;
    htim3_t.Init.AutoReloadPreload            = TIM_AUTORELOAD_PRELOAD_DISABLE;
    HAL_TIM_Base_Init(&htim3_t);
    sClockSourceConfig.ClockSource            = TIM_CLOCKSOURCE_INTERNAL;
    if(HAL_TIM_ConfigClockSource(&htim3_t, &sClockSourceConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger         = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode             = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_TIMER2_Init
  * This function configures Timer3 peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_TIMER2_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim2_t.Instance                          = TIM2;
    if(HAL_TIM_Clk_Init(&htim2_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    htim2_t.Init.Prescaler                    = 49999;
    htim2_t.Init.CounterMode                  = TIM_COUNTERMODE_UP;
    htim2_t.Init.Period                       = 899;
    htim2_t.Init.ClockDivision                = TIM_CLOCKDIVISION_DIV1;
    htim2_t.Init.AutoReloadPreload            = TIM_AUTORELOAD_PRELOAD_DISABLE;
    HAL_TIM_Base_Init(&htim2_t);
    sClockSourceConfig.ClockSource            = TIM_CLOCKSOURCE_INTERNAL;
    if(HAL_TIM_ConfigClockSource(&htim2_t, &sClockSourceConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger         = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode             = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim2_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/