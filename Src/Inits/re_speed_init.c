/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_speed_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_speed_init.h"

/**
  * @Brief RE_speed_Init
  * This function initialises the GPIO pins used to speed
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_speed_Init(void)
{
    GPIO_InitTypeDef speedsignal;
    __HAL_RCC_GPIOA_CLK_ENABLE();
     /*
     * PA4  ------> Speed signal
     */         
     speedsignal.Pin        = GPIO_PIN_4;
     speedsignal.Mode       = GPIO_MODE_IT_FALLING;
     speedsignal.Speed      = GPIO_SPEED_FAST;
     speedsignal.Pull       = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOA, &speedsignal);
     
     HAL_NVIC_SetPriority(EXTI4_IRQn,0,0);
     HAL_NVIC_EnableIRQ(EXTI4_IRQn);
     return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/