/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_thc_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_thc_init.h"

/**
  * @Brief RE_thc_Init
  * This function initialises the GPIO pins used to thermal cutoff
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_thc_Init(void)
{
    GPIO_InitTypeDef thcCtrl, thcstatus;
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
     /*
     * PB14  ------> Thermal Cutoff Control
     * PC2   ------> Thermal Cutoff Status
     */         
     thcCtrl.Pin           = GPIO_PIN_14;
     thcCtrl.Mode          = GPIO_MODE_OUTPUT_PP;
     thcCtrl.Speed         = GPIO_SPEED_FAST;
     thcCtrl.Pull          = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &thcCtrl);
     
     thcstatus.Pin         = GPIO_PIN_2;
     thcstatus.Mode        = GPIO_MODE_IT_RISING_FALLING;
     thcstatus.Speed       = GPIO_SPEED_FAST;
     thcstatus.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOC, &thcstatus);
     
     HAL_NVIC_SetPriority(EXTI2_IRQn,0,0);
     HAL_NVIC_DisableIRQ(EXTI2_IRQn);    
     return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/