 /**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_gear_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_gear_init.h"

/**
  * @Brief RE_gear_Init
  * This function initialises the GPIO pins used by gear position sensor
  * @Param None
  * @Retval Exit Status
  */
RE_StatusTypeDef RE_gear_Init (void)
{
    GPIO_InitTypeDef GDirection_pinA, GDirection_pinB ;
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /*
    * PB6 : Gear Direction PinA
    * PB7 : Gear Direction PinB
    */
     GDirection_pinA.Pin       = GPIO_PIN_6;
     GDirection_pinA.Mode      = GPIO_MODE_INPUT;
     GDirection_pinA.Speed     = GPIO_SPEED_FAST;
     GDirection_pinA.Pull      = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &GDirection_pinA);

     GDirection_pinB.Pin       = GPIO_PIN_7;
     GDirection_pinB.Mode      = GPIO_MODE_OUTPUT_PP;
     GDirection_pinB.Speed     = GPIO_SPEED_FAST;
     GDirection_pinB.Pull      = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &GDirection_pinB);
     return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/