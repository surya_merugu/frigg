/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_uart_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_uart_init.h"

UART_HandleTypeDef huart2_t;

static RE_StatusTypeDef RE_UART_GpioInit(UART_HandleTypeDef *huart);

/**
  * @Brief RE_UART_GpioInit
  * This function initialises the GPIO's used by UART peripheral
  * @Param huart: UART_HandleTypeDef
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_UART_GpioInit(UART_HandleTypeDef *huart)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if (huart->Instance == USART2)
    {
        /* Peripheral clock enable */
        __HAL_RCC_USART2_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        /*  
           PA2     ------> USART2_TX
           PA3     ------> USART2_RX 
        */
        GPIO_InitStruct.Pin         = GPIO_PIN_2 | GPIO_PIN_3;
        GPIO_InitStruct.Mode        = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull        = GPIO_PULLUP;
        GPIO_InitStruct.Speed       = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate   = GPIO_AF7_USART2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USART2_IRQn);
    }
    return RE_OK;
}

/**
  * @Brief RE_UART2_Init
  * This function configures UART2 peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_UART2_Init(void)
{
    huart2_t.Instance                 = USART2;
    huart2_t.Init.BaudRate            = 115200;
    huart2_t.Init.WordLength          = UART_WORDLENGTH_8B;
    huart2_t.Init.StopBits            = UART_STOPBITS_1;
    huart2_t.Init.Parity              = UART_PARITY_NONE;
    huart2_t.Init.Mode                = UART_MODE_TX_RX;
    huart2_t.Init.HwFlowCtl           = UART_HWCONTROL_NONE;
    huart2_t.Init.OverSampling        = UART_OVERSAMPLING_16;
    if (RE_UART_GpioInit(&huart2_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if (HAL_UART_Init(&huart2_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/