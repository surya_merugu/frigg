/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_latch_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_latch_init.h"

/**
  * @Brief RE_latch_Init
  * This function initialises the GPIO pins used by dock latch
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_latch_Init(void)
{
    GPIO_InitTypeDef LatchCtrl, Latchstatus, Status_Led;
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
     /*
     * PA9  ------> Latch control
     * PC0  ------> Latch Status
     * PB12 ------> Status Led
     */       
     LatchCtrl.Pin         = GPIO_PIN_9;
     LatchCtrl.Mode        = GPIO_MODE_OUTPUT_PP;
     LatchCtrl.Speed       = GPIO_SPEED_FAST;
     LatchCtrl.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOA, &LatchCtrl);
     
     Latchstatus.Pin       = GPIO_PIN_0;
     Latchstatus.Mode      = GPIO_MODE_IT_RISING_FALLING;
     Latchstatus.Speed     = GPIO_SPEED_FAST;
     Latchstatus.Pull      = GPIO_NOPULL;
     HAL_GPIO_Init(GPIOC, &Latchstatus);
     
     Status_Led.Pin       = GPIO_PIN_12;
     Status_Led.Mode      = GPIO_MODE_OUTPUT_PP;
     Status_Led.Speed     = GPIO_SPEED_FAST;
     Status_Led.Pull      = GPIO_NOPULL;
     HAL_GPIO_Init(GPIOB, &Status_Led);  
         
     HAL_NVIC_SetPriority(EXTI0_IRQn,0,1);
     HAL_NVIC_DisableIRQ(EXTI0_IRQn);
     return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/