/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_location_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_location_init.h"

/**
  * @Brief RE_location_Init
  * This function initialises the GPIO pins used to detect battery pack location
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_location_Init (void)
{
    GPIO_InitTypeDef LocationPin;
    __HAL_RCC_GPIOC_CLK_ENABLE();
     /*
     * PC6  ------> Location_B1
     * PC7  ------> Location_B2
     * PC8  ------> Location_B3
     * PC9  ------> Location_B4
     */  
     LocationPin.Pin        = GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9;
     LocationPin.Mode       = GPIO_MODE_OUTPUT_PP;
     LocationPin.Speed      = GPIO_SPEED_FAST;
     LocationPin.Pull       = GPIO_NOPULL;
     HAL_GPIO_Init(GPIOC, &LocationPin);
     return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/