/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_std_def.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_std_def.h"

/**
  * @Brief RE_Error_Handler
  * This function is Error handler for the application code
  * @Param pfile Pointer to the file name where the error occured
  * @Param line Line number at which the error occured
  * @Retval void
  */
void RE_Error_Handler (const char* pfile, uint16_t line)
{
    
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/