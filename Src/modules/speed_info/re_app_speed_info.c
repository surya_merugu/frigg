/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_speed_info.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "speed_info/re_app_speed_info.h"

uint32_t kmph;
uint32_t RpmCnt;
uint32_t pulse_counter;
double save_odo;
uint32_t ODO;

/**
 * @Brief RE_speed
 * This function calculates the speed of the motor
 * @Param  None
 * @Retval kmph
 */
uint32_t RE_speed(void)
{
    kmph = (uint8_t)((1.6101 * RpmCnt * 2 - 3.5347) * 50 / 653);
    return can_tx_msg[1] = kmph;
}

/**
 * @Brief RE_distance_covered
 * this function checks for the ODO
 * @Param None
 * @Retval odo
 */
RE_StatusTypeDef RE_distance_covered(void)
{   
    save_odo += (double) kmph/7200;
    if (save_odo >= 1) 
    {
        save_odo = 0;
        ODO++;
        SystemConfig.odo[3]  = (ODO & 0xFF);
        SystemConfig.odo[2]  = ((ODO >> 8)  & 0xFF);
        SystemConfig.odo[1]  = ((ODO >> 16) & 0xFF);
        SystemConfig.odo[0]  = ((ODO >> 24) & 0xFF);    
        if(RE_Transmit_odo() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        if(RE_WriteOdoToNVS(SystemConfig.odo) != RE_OK)   
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
    }
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/