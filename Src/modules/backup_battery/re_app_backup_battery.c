/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_location.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "backup_battery/re_app_backup_battery.h"

static bool Voltage1 = false, Voltage2 = false, Voltage3 = false;
uint8_t Backup_BatVoltage;

/**
  * @Brief RE_BackupBat_Voltage_Detection
  * This function checks for the backup battery voltage level detection
  * @Param None
  * @Retval Battery left
  */
uint8_t RE_BackupBat_Voltage_Detection (void)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);
    Voltage1 = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3);
    Voltage2 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_4);
    Voltage3 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_5);
    if(Voltage1 == true && Voltage2 == true && Voltage3 == true)
    {
        return Backup_BatVoltage = (uint8_t)75;
    }
    else if(Voltage1 == false && Voltage2 == true && Voltage3 == true)
    {
        return Backup_BatVoltage = (uint8_t)50;
    }
    else if(Voltage1 == false && Voltage2 == false && Voltage3 == true)
    {
        return Backup_BatVoltage = (uint8_t)25;
    }
    else
    {
        return Backup_BatVoltage = (uint8_t)0;
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/