/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_key_position.c
  * Origin Date           :   10/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "key_position/re_app_key_position.h"

bool tx_key_status_Flag;

/**
 * @Brief RE_TurnON_key
 * this function turns ON relay used by key
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_TurnON_key (void)
{       
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
    return RE_OK;
}

/**
 * @Brief RE_TurnOFF_key
 * This function turns OFF relay used by key
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_TurnOFF_key (void)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/