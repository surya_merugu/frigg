/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_idle_state.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes*/
#include "idle_state/re_app_idle_state.h"

static uint8_t latch_close_timer = 0;
uint8_t status;

/**
 * @Brief RE_Idle_State_Handler
 * this function handles tasks in CPU idle mode
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Idle_State_Handler(void)
{
    if(t500ms_Elapsed == true)
    {
        if(HAL_TIM_Base_Stop_IT(&htim3_t) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
//        HAL_NVIC_DisableIRQ(EXTI4_IRQn);
        if(latch_opened == true)
        {
            latch_close_timer++;
        }
        if(latch_close_timer == 2)
        {
            if(RE_Close_DockLatch() != RE_OK)
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
            latch_opened = false;
            latch_close_timer = 0;
        }
        RE_speed();
        if(RE_distance_covered() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        SystemConfig.latch_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, (GPIO_PinState)SystemConfig.latch_status);
        RE_WriteBytetoNVS(6, SystemConfig.latch_status);
        SystemConfig.Thc_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);        
        RE_WriteBytetoNVS(7, SystemConfig.NoofPacksDetected);
        if(RE_Transmit_Frequent_Data() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        RE_BackupBat_Voltage_Detection();
        t500ms_Elapsed = false;
        if(HAL_TIM_Base_Start_IT(&htim3_t) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
//        HAL_NVIC_EnableIRQ(EXTI4_IRQn);
    }
    if(tx_key_status_Flag == true)
    {
        if(RE_Transmit_key_Detection() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        tx_key_status_Flag = false;
    }
    if(tx_latch_status_Flag == true)
    {
        if(RE_Transmit_Latch_Status() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        tx_latch_status_Flag = false;
    }
    if(Uart2RxCmplt == true)
    {
        if(RE_UART2_RxDataHandler() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        Uart2RxCmplt = false;
    }
    if(Tx_SysConfigureData_Flag == true)
    {
        if(RE_Transmit_SysConfigureData() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        Tx_SysConfigureData_Flag = false;
    }
    if(tx_thc_Flag == true)
    {
        if(RE_Transmit_Latch_Status() != RE_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
        tx_thc_Flag = false;
    }
    return RE_OK;
}

RE_StatusTypeDef RE_Load_System_Status(void)
{
    SystemConfig.latch_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
    if(RE_WriteBytetoNVS(6, SystemConfig.latch_status) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    SystemConfig.key_status   = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);
    if(RE_WriteBytetoNVS(4, SystemConfig.key_status) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    SystemConfig.Thc_status   = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);
    if(RE_WriteBytetoNVS(8, SystemConfig.Thc_status) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    RE_Packs_Detection();
    if(RE_WriteBytetoNVS(7, SystemConfig.NoofPacksDetected) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/