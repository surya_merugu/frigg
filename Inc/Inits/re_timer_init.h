/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_timer_init.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None 
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_TIMER_INIT_H
#define _RE_TIMER_INIT_H

/* Includes */
#include "re_std_def.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_tim.h"
#include "stm32f4xx_it.h"

extern TIM_HandleTypeDef htim3_t, htim2_t;

/* Exported API */
RE_StatusTypeDef RE_TIMER3_Init(void);
RE_StatusTypeDef RE_TIMER2_Init (void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/