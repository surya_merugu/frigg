/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_idle_state.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_IDLE_STATE_H
#define __RE_IDLE_STATE_H

#include "re_std_def.h"
#include "re_can_int_callback.h"
#include "re_timer_int_callback.h"
#include "re_gpio_int_callback.h"
#include "re_can_init.h"
#include "can/re_app_can.h"
#include "gear_status/re_app_gear_status.h"
#include "speed_info/re_app_speed_info.h"
#include "location/re_app_location.h"
#include "backup_battery/re_app_backup_battery.h"
#include "motor_temperature/re_app_motor_temperature.h"
#include "dock_latch/re_app_dock_latch.h"
#include "key_position/re_app_key_position.h"
#include "uart/re_app_UART_Ring.h"

RE_StatusTypeDef RE_Idle_State_Handler(void);
RE_StatusTypeDef RE_Load_System_Status(void);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/