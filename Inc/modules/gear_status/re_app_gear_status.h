/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_gear_status.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_GEAR_STATUS_H
#define _RE_APP_GEAR_STATUS_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "re_std_def.h"
#include "stm32f4xx_it.h"
#include "re_gear_init.h"
#include "can/re_app_can.h"

uint8_t RE_Gear_Direction (void);
RE_StatusTypeDef RE_Turn_ON_RevLight(void);
RE_StatusTypeDef RE_Turn_OFF_RevLight(void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/