/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_location.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_LOCATION_H
#define _RE_APP_LOCATION_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "re_std_def.h"
#include "stm32f4xx_it.h"
#include "re_gear_init.h"
#include "can/re_app_can.h"
#include "stdbool.h"
#include "nvs/re_app_nvs.h"

extern bool tx_pack_detection_Flag;

RE_StatusTypeDef RE_Packs_Detection (void);
RE_StatusTypeDef RE_TurnON_LocationPins(void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/