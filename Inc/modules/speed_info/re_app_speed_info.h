/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_speed_info.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_SPEED_INFO_H
#define _RE_APP_SPEED_INFO_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "re_std_def.h"
#include "can/re_app_can.h"
#include "nvs/re_app_nvs.h"

extern uint32_t kmph;
extern uint32_t RpmCnt;
extern uint32_t ODO;
extern uint32_t pulse_counter;

uint32_t RE_speed(void);
RE_StatusTypeDef RE_distance_covered(void);

#endif
/**************************** END OF FILE *************************************/