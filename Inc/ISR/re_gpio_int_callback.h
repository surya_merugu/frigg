/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_gpio_int_callback.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/******** Define to prevent recursive inclusion*****************************/
#ifndef _RE_GPIO_INT_CALLBACK_H
#define _RE_GPIO_INT_CALLBACK_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_it.h"
#include "re_std_def.h"
#include "speed_info/re_app_speed_info.h"
#include "dock_latch/re_app_dock_latch.h"
#include "key_position/re_app_key_position.h"
#include "motor_temperature/re_app_motor_temperature.h"

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/