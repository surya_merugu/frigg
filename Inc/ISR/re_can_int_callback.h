/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_CAN_INT_CALLBACK_H
#define _RE_CAN_INT_CALLBACK_H

/* Includes */
#include "main/main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"
#include "re_std_def.h"
#include "re_can_init.h"
#include "stdbool.h"
#include "dock_latch/re_app_dock_latch.h"
#include "location/re_app_location.h"

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/